from flask import Flask, render_template, request, jsonify
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from datetime import datetime, timedelta
import random

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://mongo:27017/yourdatabase"
mongo = PyMongo(app)
db = mongo.db

# Prepopulare cu 200 de intrări

tags_list = ["fundatie", "zidarie", "acoperis", "finisaje", "electrice", "sanitare",
                "izolatie", "tencuiala", "parchet", "gresie", "faianta", "vopsit", "gard", "geamuri", "usi",
                "mobilier", "curte", "terasa", "mansarda", "demisol"]

if db.journal.count_documents({}) == 0:
    for i in range(200):
        entry = {
            "lucrare": f"Lucrare exemplu {i}",
            "executant": f"Executant exemplu {i}",
            "plata": random.randint(0, 1000),
            "etichete": random.sample(tags_list, random.randint(1, 5)),
            "data": datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=random.randint(0, 1095))  # Date random în ultimii 3 ani
        }
        db.journal.insert_one(entry)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/add', methods=['POST'])
def add_entry():
    data = request.get_json()

    # Convert entry_date to a datetime object
    try:
        data['data'] = datetime.strptime(data['data'], "%Y-%m-%d")
    except ValueError:
        return jsonify({'status': 'failure', 'reason': 'Invalid date format'}), 400

    db.journal.insert_one(data)
    return jsonify(success=True)

@app.route('/history', methods=['GET'])
def get_history():
    filter_tags = request.args.get('tags')
    query = {}
    if (filter_tags):
        tags_list = [tag.strip() for tag in filter_tags.split(',')]
        query['etichete'] = {'$in': tags_list}

    journal_entries = list(mongo.db.journal.find(query).sort('data', -1))
    for entry in journal_entries:
        entry['_id'] = str(entry['_id'])
        entry['data'] = entry['data'].strftime("%Y-%m-%d")
    return jsonify({'entries': journal_entries}), 200

@app.route('/update_entry', methods=['POST'])
def update_entry():
    data = request.json
    entry_id = data.get('id')
    update_data = {
        'data': datetime.strptime(data.get('data'), '%Y-%m-%d'),
        'lucrare': data.get('lucrare'),
        'executant': data.get('executant'),
        'plata': data.get('plata', 0),  # Default to 0 if not provided
        'etichete': data.get('etichete')
    }

    # Verifică dacă documentul există
    if not mongo.db.journal.find_one({'_id': ObjectId(entry_id)}):
        app.logger.error(f"Document with ID {entry_id} does not exist.")
        return jsonify({"error": "Document does not exist."}), 404

    result = mongo.db.journal.update_one({'_id': ObjectId(entry_id)}, {'$set': update_data})

    if result.modified_count != 1:
        error = f"Error updating entry {entry_id}"
        app.logger.error(error)
        return jsonify({'status': 'error', 'message': error}), 500

    return jsonify({'status': 'success'}), 200

@app.route('/tags', methods=['GET'])
def get_tags():
    # Obține toate etichetele distincte din colecția journal
    tags = db.journal.distinct("etichete")
    return jsonify(tags=tags)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
