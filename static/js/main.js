document.addEventListener("DOMContentLoaded", function () {
  const tagInput = document.getElementById("etichete");
  const filterTagsInput = document.getElementById("filter-tags");

  fetch('/tags')
    .then(response => response.json())
    .then(data => {
      const tags = data.tags;
      new Awesomplete(tagInput, { list: tags, filter: Awesomplete.FILTER_CONTAINS, item: Awesomplete.ITEM });
      new Awesomplete(filterTagsInput, { list: tags, filter: Awesomplete.FILTER_CONTAINS, item: Awesomplete.ITEM });
    });

  document.getElementById("reset-filter").addEventListener("click", function () {
    filterTagsInput.value = '';
    loadHistory();
  });

  // Clear filter input on page load
  filterTagsInput.value = '';
  loadHistory();

  flatpickr("#data", {
    defaultDate: "today"
  });
});

function submitEntry() {
  var lucrare = document.getElementById("lucrare").value;
  var executant = document.getElementById("executant").value;
  var plata = document.getElementById("plata").value.trim();  // Obține valoarea câmpului "Plătit" și elimină spațiile
  var etichete = document.getElementById("etichete").value.split(",").map(tag => tag.trim()).filter(tag => tag).sort();
  var data = document.getElementById("data").value;

  // Verifică dacă câmpul "Plătit" este gol și setează valoarea implicită la 0
  if (plata === '') {
    plata = 0;
  }

  var entryData = {
    lucrare: lucrare,
    executant: executant,
    plata: plata,
    etichete: etichete,
    data: data
  };

  fetch('/add', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(entryData),
  })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
      console.log('Răspuns de la server:', data);
      // Resetează formularul după trimiterea cu succes a datelor
      document.getElementById("entry-form").reset();
      loadHistory();  // Reîncarcă istoricul după adăugarea unei noi intrări
    })
    .catch(error => {
      console.error('Eroare în timpul trimiterii datelor către server:', error);
    });
}

function loadHistory() {
  const filterTagsInput = document.getElementById("filter-tags").value.trim();
  let url = '/history';

  if (filterTagsInput) {
    url += `?tags=${filterTagsInput}`;
  }

  fetch(url)
    .then(response => response.json())
    .then(data => {
      const historyTable = document.getElementById("history-table").getElementsByTagName("tbody")[0];
      historyTable.innerHTML = "";

      data.entries.forEach(entry => {
        let tr = document.createElement("tr");

        tr.innerHTML = `
                  <td class="data">${entry.data}</td>
                  <td class="lucrare">${entry.lucrare}</td>
                  <td class="executant">${entry.executant}</td>
                  <td class="plata">${entry.plata}</td>
                  <td class="etichete">${entry.etichete.sort().map(tag => `<span class="tag">${tag}</span>`).join("")}</td>
                  <td>
                      <input type="hidden" class="entry-id" value="${entry._id}">
                      <button class="edit-btn" onclick="editEntry(this)">✏️</button>
                      <button class="save-btn" onclick="saveEntry(this)" style="display:none;">✔️</button>
                      <button class="cancel-btn" onclick="cancelEdit(this)" style="display:none;">❌</button>
                  </td>
              `;
        historyTable.appendChild(tr);
      });
    })
    .catch(error => {
      console.error('Eroare la încărcarea istoricului:', error);
    });
}

function editEntry(button) {
  hideEditButtonForAll(true)

  const tr = button.closest("tr");
  const data = tr.querySelector(".data").innerText;
  const lucrare = tr.querySelector(".lucrare").innerText;
  const executant = tr.querySelector(".executant").innerText;
  const plata = tr.querySelector(".plata").innerText;
  const etichete = Array.from(tr.querySelectorAll(".etichete .tag")).map(tag => tag.innerText).join(", ");

  tr.querySelector(".data").innerHTML = `<input type="text" class="data-input" value="${data}">`;
  flatpickr(tr.querySelector(".data-input"), { defaultDate: data });
  tr.querySelector(".lucrare").innerHTML = `<input type="text" class="lucrare-input" value="${lucrare}">`;
  tr.querySelector(".executant").innerHTML = `<input type="text" class="executant-input" value="${executant}">`;
  tr.querySelector(".plata").innerHTML = `<input type="number" class="plata-input" value="${plata}">`;
  tr.querySelector(".etichete").innerHTML = `<input type="text" class="etichete-input" value="${etichete}">`;

  button.style.display = "none";
  tr.querySelector(".save-btn").style.display = "inline-block";
  tr.querySelector(".cancel-btn").style.display = "inline-block";
}

function saveEntry(button) {
  const tr = button.closest("tr");
  const entryId = tr.querySelector(".entry-id").value;
  const updatedEntry = {
    id: entryId,
    data: tr.querySelector(".data-input").value,
    lucrare: tr.querySelector(".lucrare-input").value,
    executant: tr.querySelector(".executant-input").value,
    plata: tr.querySelector(".plata-input").value || 0,
    etichete: tr.querySelector(".etichete-input").value.split(",").map(tag => tag.trim()).filter(tag => tag).sort()
  };

  fetch('/update_entry', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(updatedEntry)
  })
    .then(response => response.json())
    .then(data => {
      if (data.status === 'success') {
        loadHistory();
        hideEditButtonForAll(false)
      } else {
        alert('Eroare la actualizarea intrării');
      }
    })
    .catch(error => {
      console.error('Eroare la actualizarea intrării:', error);
    });
}

function cancelEdit(button) {
  const tr = button.closest("tr");
  const data = tr.querySelector(".data-input").defaultValue;
  const lucrare = tr.querySelector(".lucrare-input").defaultValue;
  const executant = tr.querySelector(".executant-input").defaultValue;
  const plata = tr.querySelector(".plata-input").defaultValue;
  const etichete = tr.querySelector(".etichete-input").defaultValue.split(",").map(tag => tag.trim()).sort();

  tr.querySelector(".data").innerText = data;
  tr.querySelector(".lucrare").innerText = lucrare;
  tr.querySelector(".executant").innerText = executant;
  tr.querySelector(".plata").innerText = plata;
  tr.querySelector(".etichete").innerHTML = etichete.map(tag => `<span class="tag">${tag}</span>`).join("");

  tr.querySelector(".edit-btn").style.display = "inline-block";
  tr.querySelector(".save-btn").style.display = "none";
  tr.querySelector(".cancel-btn").style.display = "none";

  hideEditButtonForAll(false)
}

function hideEditButtonForAll(hide) {
  const allEditButtons = document.querySelectorAll(".edit-btn");
  if (hide) {
    allEditButtons.forEach(btn => btn.style.display = "none");
  } else {
    allEditButtons.forEach(btn => btn.style.display = "inline-block");
  }
}
