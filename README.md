# Jurnal de Șantier


## TO DO

* ~~date picker la adăugare intrare~~
* ~~Posibilitatea de editare intrare~~
* posibilitatea de ștergere intrare
* ~~Afișat etichete ca etichete, nu doar stringuri~~
* Folosit tab pentru autocompletare etichete - ia prima sugestie
* export și import la baza de date - backup
* convertire date din MarkDown
* Convertire date din XLS/CSV
* https://coreui.io/ sau https://mui.com/

* Paginație la istoric
  * încarcă doar ultimele X intrări
    * încarcă mai multe la cerere
  * paginile sunt pe luni și apoi ani
    * arbore pentru fiecare nivel

* tag cloud

* filă nouă: costuri
  * Arată totalul, implicit
  * Filtrare după etichete - implicit, orice etichetă
  * Filtrare după interval de timp - implicit, tot timpul

* Grafic de cheltuieli în timp
* Grafic de cât timp s-a lucrat și cât timp a fost pauză
* Sumar de cât timp s-a lucrat
  * Arată totalul, implicit
  * Filtrare după etichete - implicit, orice etichetă
  * Filtrare după interval de timp - implicit, tot timpul
